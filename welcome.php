<?php
   include('lock.php');
   include('paieska.php');
?>
<!DOCTYPE HTML>
<html>
   <head>
      <title>Welcome </title>
 
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link rel="stylesheet" href="assets/css/main.css" />
    </head>
    <body class="landing">   
        <div id="page-wrapper">
        
    <header id="header">
	<center>
	<div class="container">

    <h2>Welcome, <?php echo $login_session; ?></h2>      

    </div>
		
			<?php
			$TimeZone = date_default_timezone_get('Europe/Vilnius');

					date_default_timezone_set($TimeZone);
					echo "  " . date("d-m-y");
					echo "  " . date("h:i:s");
					echo "  " . date("l");
					echo "  Good Evening: ".$login_session;					
			?>
                <nav id="nav">
                    <ul>
                        <li><a href="logout.php" class="button special">Atsijungti</a></li>
                    </ul>
                </nav>
	</center>
    </header>

<section id="four" class="wrapper style1 special fade-up">
	
  	<div>
	<form>
	<tr>
	<th><h1>Nuotraukos pavadinimas <input type="text" name = "fotopav"></h1></th>    
	</tr>
	</form>
	<table width="274" border="0" align="center" cellpadding="2" cellspacing="0">	  
	<form method = "post" action = "perziura.php" enctype="multipart/form-data">

	<tr>
	<td><input type="Submit" value = "Fotkiu perziura" name = "ziureti"></td>
	</tr>
	</form>
	
	<form action = "pav_db.php" method = "post" enctype="multipart/form-data">
	<tr>
	<td><input name="image" id="image" accept="image/jpeg" type="file"></td>    
	</tr>
	<tr>  
	<td><input type="Submit" value = "ikelti"></td>    
	</tr>
	</form>
	</table>
	</div>
</section> 

</body>
</html>