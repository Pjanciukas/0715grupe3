<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Registracija</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css" />
    </head>
    <body class="landing">   
        <div id="page-wrapper">

            <!-- Header -->
    <header id="header">
              
                <nav id="nav">
                    <ul>
                        <li><a href="login.php" class="button special">Prisijungimas</a></li>
                    </ul>
                </nav>
            </header>

            <!-- Four -->
            <section id="four" class="wrapper style1 special fade-up">
                <div class="container">
                    <header class="major">
                        <h2>Registracija</h2>
                        
					<center>
					<form method="get" action="insert.php">
						<table width="274" border="0" align="center" cellpadding="2" cellspacing="0">
  
                            <div class="collomn uniform 50%">
                            	<div class="6u$ 12u$(xsmall)">
                                    <input type="text" name="username"  placeholder="vartotojo vardas" />
                                </div>
                                <div class="6u$ 12u$(xsmall)">
                                    <input type="text" name="firstname"  placeholder="vardas" />
                                </div>
								<div class="6u$ 12u$(xsmall)">
									<input type="text" name="lastname"  value="" placeholder="pavarde" />
									</div>
                                <div class="6u$ 12u$(xsmall)">
                                    <input type="email" name="email"  value="" placeholder="el_pastas" />
                                </div>
								<div class="6u$ 12u$(xsmall)">
									<input type="password" name="password"  value="" placeholder="slaptazodis" />
								</div>
                                <div class="12u$">
                                    <ul class="actions">
                                        <li><input type="submit" value="Registruotis" class="special" /></li>
                                    </ul>
                                </div>
                            </div>
                        </form>
						</center>
                    </header>
                </div>
            </section>
        
      </body>
</html>       				